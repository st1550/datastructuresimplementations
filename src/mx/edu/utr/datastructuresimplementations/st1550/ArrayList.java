package mx.edu.utr.datastructuresimplementations.st1550;

import java.util.Scanner;

public class ArrayList implements List {

    private Object[] elements;
    private int size = 0;

    /*
    This method will initialize the arrayList creating it with the defined parameter for size
     */
    public ArrayList(int initialCapacity) {
        elements = new Object[initialCapacity];
    }

    /*
    This is an auxiliar method that helps to validate if the ArrayList has reached it's full capacity
    if Array's length is equal to size it will store temporarily using "old" this array and create a new one with double (+1) capacity
    once new array is created all the information will be copied from "old" to the new array using the same name.
     */
    private void ensureCapacity() {
        if (elements.length == size) {
            Object[] old = elements;
            elements = new Object[2 * size + 1];
            System.arraycopy(old, 0, elements, 0, size);
        }
    }

    @Override
    /*This method receives a new object to be inserted on the arraylist 
      and stores that value on the next available field on the arraylist
    this method uses ensureCapacity to make sure ArrayList has enough space to store more elements
     */
    public boolean add(Object o) {
        ensureCapacity();
        elements[size++] = o;
        return true;
    }

    /*
    This method ensures provided element is inserted on specific position
    it uses ensureCapacity method to verify if array is capable to take a new element
    either on the same array or a new one with more capacity it starts a for iteration 
    to move all objects FROM the specified index to the right of that index so the space 
    for index is not filled, once these objects were moved the provided value is added on requested index value --mueve todos los objetos a la derecha de donde se quiere poner el nuevo objeto y al final guarda el objecto dado en el espacio dado
     */
    @Override
    public void add(int index, Object o) {
        ensureCapacity();

        for (int i = size - 1; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = o;

        size++;
    }

    /*
    This method will take the current ArrayList size and creates a new Arraylist with the same size but without values.
     */
    @Override
    public void clear() {
        int sameSize = elements.length;
        elements = new Object[sameSize];
    }

    /*
    Based on received parameter this method will return what is contained on that position of the array.
     */
    @Override
    public Object get(int index) {
        return elements[index];
    }

    /*
      
   This method receives an index parameter which will compare against the elements on the arraylist
    and will return the position where this element is found
    return the index value of the array where provided value was found.
     */
    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(elements[i])) {
                return i;
            }
        }
        return -1;
    }

    /*
    This method verifies if ArrayList size is 0
    it returns the result of this validation either true or false.
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /*
    Based on provided index value it will start a for iteration in which 
    will move all the elements of the array one space back from the provided 
    value, which represents overwritting the specified index element for the elements after it
    once this overwritting process ends it will return the new ArrayList in which desired element is not present.
     */
    @Override
    public Object remove(int index) {
        Object o = elements[index];

        for (int j = index; j < size - 1; j++) {
            elements[j] = elements[j + 1];
        }
        size--;
        return o;
    }
//This method returns the number of elements in this ArrayList.

    @Override
    public int size() {
        return size;
    }

    /*
    This is a support method to verify range provided by user
    this method will be invoked by other methods to verify if desired index is in or out of range
    if it's out of range it will return a message saying provided range is out of range.
     */
    private void rangeCheck(int index) {
        if (index >= size) {
            System.out.println("Out of range!");
        }
    }

    /*
    gathers 2 parameters 
    "index" defines where in the Array the provided element will be set
    "o" will be the new element stored in the specified space defined in index
    it will return a copy of the existing data plus the added element
     */
    @Override
    public Object set(int index, Object o) {
        Object old = elements[index];
        elements[index] = o;
        return old;
    }

    public static void main(String[] args) {
        //I've created this main to perform my own tests using testArray.
        int pos = 0, collect;
        boolean check;
        String data;
        Scanner sc = new Scanner(System.in);

        System.out.print("Provide Array's initial size: ");
        collect = sc.nextInt();
        ArrayList testArray = new ArrayList(collect);
        System.out.println("Is this array Empty?: " + testArray.isEmpty());

        while (collect > 0) {
            System.out.print("Value for position " + pos + ": ");
            data = sc.next();
            testArray.set(pos, data);
            collect--;
            pos++;
        }
        System.out.println("Array size: " + testArray.size());
        System.out.print("Array element to retrive: ");
        collect = sc.nextInt();
        System.out.print("Position " + collect + ", contains: " + testArray.get(collect));
        System.out.println("Want to clear Array's content? [true/false:] ");
        check = sc.nextBoolean();
        if (check == true) {
            testArray.clear();
            System.out.println("Array was cleaned");
            System.out.println("Array's size: " + testArray.size());
        } else {
            System.out.println("Array's size: " + testArray.size());
        }
    }
}
