package mx.edu.utr.datastructuresimplementations.st1550;

public class LinkedList implements List {

    private int size = 0;
    private Node first, last;

    /* 
    Constructor without parameters to allow its use
     */
    public LinkedList() {
    }

    /*
    This methods add an element on the top of the list (next index of the list available).
     */
    @Override
    public boolean add(Object o) {
        final Node l = last;
        final Node newNode = new Node(l, o, null);
        last = newNode;
        if (l == null) {
            first = newNode;
        } else {
            l.next = newNode;
        }
        size++;
        return true;
    }

    /*
    This method receives both index and element to be inserted  on the list
    verifies if index exist, if it's found it moves all the elements to the right and assigns provided value on index given
    if it's out of range it adds it to the next available position.
     */
    @Override
    public void add(int index, Object element) {
        Node aux = first;
        for (int i = 0; i < index; i++) {
            if (aux != null) {
                aux = aux.next;
            } else {
                System.out.println("List is not big enough, element inserted on index " + (i + 1));
                break;
            }
        }
        Node newNode = new Node(aux.prev, element, aux);
        aux.prev.next = newNode;
        aux.prev = newNode;
    }

    /*
    This method will equal first and last to null therefore list will contain no elements.
     */
    @Override
    public void clear() {
        first = last = null;
        size = 0;

    }

    /*
    This method verifies provided index against the elements 
    on the list and will return the value contained
     */
    @Override
    public Object get(int index) {
        Node aux = first;
        for (int i = 0; i < index; i++) {
            if (aux != null) {
                System.out.println("There's not enough elements");
                return null;
            }
        }
        return aux.item;
    }

    /*
    This method verifies provided element against the elements on the list and will return the index value
     */
    @Override
    public int indexOf(Object o) {
        Node aux = first;
        for (int i = 0; i < size; i++) {
            if (aux.item == o) {
                return i;
            } else {
                aux = aux.next;
            }
        }
        return 0;
    }
//this validation will check if List is empty and return either true or false based on that validation

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /*
    This method will take user provided size and if its on range 
    will remove choosen element and more remaining elements to the right overwritting that element
     */
    @Override
    public Object remove(int index) {
        Node aux = first;
        for (int i = 0; i > index; i++) {
            if (aux == null) {
                System.out.println("Out of index range!");
                return null;
            } else {
                aux = aux.next;
            }
        }
        Node previous = aux.prev;
        previous.next = null;
        size--;
        return previous;
    }

    /*
    This method will be moving elements to the right if provided index value is on range
     */
    @Override
    public Object set(int index, Object element) {
        Node aux = first;
        for (int i = 0; i > index; i++) {
            if (aux == null) {
                System.out.println("Out of index range!");
                return null;
            } else {
                aux = aux.next;
            }
        }
        Node newNode = new Node(aux.prev, element, aux.prev);
        aux.prev.next = newNode;
        aux.next.prev = newNode;
        return aux.item;
    }

    /*
    this method returns the current size of the linkedlist
     */
    @Override
    public int size() {
        return size;
    }

}

/*
This class will create nodes to integrate a list with its own pointers to make refence,
item is its own value, next and prev have to be difined as its neiborgh vlues
 */
class Node {

    Node next;
    Object item;
    Node prev;

    public Node(Node prev, Object item, Node next) {
        this.next = next;
        this.item = item;
        this.prev = prev;
    }
}
